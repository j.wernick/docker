# Copyright (c) 2019-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

stages:
  - "test"
  - "build"

.build:
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor
  image:
    "docker:stable"
  variables:
    DOCKER_HOST: "tcp://docker:2375/"
    DOCKER_DRIVER: "overlay2"
    GIT_SUBMODULE_STRATEGY: "recursive"
  services:
    # Use working DinD, latest is broken
    # https://gitlab.com/gitlab-com/support-forum/issues/5202
    - "docker:19.03.5-dind"
  before_script:
    - "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY"
    - "apk add --no-cache python3 py3-pip"
    - "pip install requests"
  script:
    - "image=$CI_REGISTRY/$CI_PROJECT_PATH/$CONTAINER:$VERSION"
    # the docker_image_token_v2 is a new (hidden version) of the original
    # yamllint disable rule:line-length
    - "docker build -t $image $CONTAINER/$VERSION --build-arg docker_image_token_v2=$docker_image_token_v2"
    # yamllint enable rule:line-length
    - |
        if [ -f $CONTAINER/$VERSION/cs-tests.yml ] ; then
          .utilities/container-structure-test-linux-amd64 \
            test --image ${image} \
            --config $CONTAINER/$VERSION/cs-tests.yml
        fi
    # Due to docker-in-docker this cannot be a separate job
    # without completely rebuilding the image
    - |
        if [ $CI_COMMIT_REF_NAME == "master" ] ; then
          docker push $image
          # Scan the Docker image using Trivy
          docker pull registry.gitlab.com/eyeo/docker/trivy:0.38.3
          docker run --rm \
              -v $PWD:/root/ \
              registry.gitlab.com/eyeo/docker/trivy:0.38.3 \
              --cache-dir /root/.cache \
              --format json -o /root/trivy-output.json \
              --exit-code 0 --severity MEDIUM,HIGH,CRITICAL \
              image $image
          # Upload the report to Defectdojo
          if [ -f trivy-output.json ]; then
              python3 ./upload_to_defectdojo.py \
              --action 'tupload' \
              --host 'eyeo.cloud.defectdojo.com' \
              --port 443 --environment 'Development' \
              --api_key $DEFECTDOJO_API_KEY --product_name 'Docker' \
              --lead_id 13 --result_file trivy-output.json \
              --scanner 'Trivy Scan' \
              --engagement_name $CONTAINER;
          fi
        else
          echo "Not pushing image from ${CI_COMMIT_REF_NAME} branch"
        fi
  after_script:
    - "docker logout $CI_REGISTRY"
  stage:
    "build"
  artifacts:
    paths: [trivy-output.json]
    when: "always"
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .cache

include:
  - local: "/images.yml"

yamllint:
  image:
    "registry.gitlab.com/eyeo/docker/yamllint:1.14.0"
  script:
    - "set -- *.yml"
    - "set -- $@ .gitlab-ci.yml"
    - "set -- $@ .yamllint.yml"
    - "set -- $@ `find * -name '*.yml'`"
    - "yamllint -c .yamllint.yml $@"
  stage:
    "test"
