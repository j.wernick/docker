# Crumbs Android Runner Docker Images

This image is used by the following Crumbs Android projects:

- [Crumbs Core](https://gitlab.com/eyeo/crumbs/crumbs-core)
- [Crumbs Android](https://gitlab.com/eyeo/crumbs/crumbs-android)
- [Crumbs ABP](https://gitlab.com/eyeo/crumbs/crumbs-abp)

## Running the emulator

This build environment provides a pre-configured Android emulator to run instrumentation tests.

To start the emulator run:

```
start-emulator.sh
```

To stop it run:

```
stop-emulator.sh
```