# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:20.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"
ARG CHROME_URL="https://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_109.0.5414.119-1_amd64.deb"
ARG CHROME_SHA256_CHECKSUM="7dfe9285ff6ee8bff7621a68bf66d33dbe016ee3769a20b52397bafa49345745"

# Adjust env.
ENV LC_CTYPE="en_US.UTF-8"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Install generic system packages
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        ca-certificates \
        ccache \
        curl \
        dumb-init \
        git \
        lsb-core \
        sudo \
        unzip \
        wget \
        ruby-full \
        npm \
        openjdk-17-jdk \
        # Emulator & video bridge dependencies
        libc6 libdbus-1-3 libfontconfig1 libgcc1 \
        libpulse0 libtinfo5 libx11-6 libxcb1 libxdamage1 \
        libnss3 libxcomposite1 libxcursor1 libxi6 \
        libxext6 libxfixes3 zlib1g libgl1 pulseaudio socat

# Install chrome
RUN wget -q ${CHROME_URL} -O /tmp/google-chrome-stable.deb \
&&  echo "${CHROME_SHA256_CHECKSUM} /tmp/google-chrome-stable.deb" | sha256sum -c \
&&  apt-get install -qy /tmp/google-chrome-stable.deb \
&&  rm /tmp/google-chrome-stable.deb

# Install jekyll
RUN gem install jekyll -v 4.2.2

# Clean
RUN rm -rf /var/lib/apt/lists/*