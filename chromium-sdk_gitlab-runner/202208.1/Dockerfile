# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
FROM ubuntu:20.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"
ARG DEPOT_TOOLS="/opt/depot_tools"

# Adjust env
ENV GOOGLE_PLAY_AGREE_LICENSE="1" \
    LC_CTYPE="en_US.UTF-8" \
    CHROME_DEVEL_SANDBOX="1" \
    DEPOT_TOOLS="$DEPOT_TOOLS"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Define some system-wide requirements that are needed
# for things other than the build to work
ARG SYSTEM_REQUIREMENTS="\
  build-essential \
  ca-certificates \
  ccache \
  curl \
  dumb-init \
  git \
  lsb-core \
  p7zip-full \
  python \
  sudo \
  unzip \
  wget \
"

#  Things which the actual build needs.
ARG BUILD_REQUIREMENTS="\
  clang \
  libc6:i386 \
  locales \
  openjdk-8-jdk-headless \
  pkg-config \
  python-setuptools \
  python3-pip \
"

# Things needed for windows build.
ARG WINDOWS_REQUIREMENTS="\
 libfuse2 \
"

# Install generic system packages before chromium specific dependencies (below the line break)
RUN dpkg --add-architecture i386 \
&& echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends $SYSTEM_REQUIREMENTS $BUILD_REQUIREMENTS $WINDOWS_REQUIREMENTS \
&&  rm -rf /var/lib/apt/lists/*

# Generate missing locale
RUN locale-gen en_US.UTF-8

# Install Chromium's depot_tools
RUN git clone --single-branch https://chromium.googlesource.com/chromium/tools/depot_tools.git $DEPOT_TOOLS

# Install Eyeo chromium build tools
ARG docker_image_token_v2
RUN git clone https://username:${docker_image_token_v2}@gitlab.com/eyeo/distpartners/eyeo-chromium-build-tools.git /opt/eyeobin/

# Install Chromium build dependencies so less is installed at build time
# Installed deps are tracked using an md5sum of the script which is read at build time
ADD install-build-deps.sh install-build-deps-android.sh /tmp/build/
RUN /tmp/build/install-build-deps-android.sh \
&&  md5sum /tmp/build/* | sed -e 's|/tmp/||g' >> /installed_deps \
&&  rm -rf /var/lib/apt/lists/* /tmp/build

# Add non-root user and make it part of sudo group
RUN useradd -ms /bin/bash non_root
RUN usermod -aG sudo non_root
# Make it possible to use sudo witout tty
RUN echo "Defaults:non_root !requiretty" >> /etc/sudoers
RUN chown -R non_root:non_root /opt/ ~ /installed_deps
