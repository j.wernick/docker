# gitlab-codequality Docker Image

This image contains team-defined code quality tools (currently [checkstyle](https://checkstyle.sourceforge.io/)) and additional tools to
convert, using [violations-command-line](https://github.com/tomasbjerre/violations-command-line), to the CodeClimate JSON format
which gitlab can parse and present to the user ([doc](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)).


## Example usage

The below will add a job to a CI pipeline that measures and presents code quality details to the user.
In this example, a project-specific checkstyle config is in `config/checkstyle/checkstyle.xml`.

```
checkstyle:
  stage: lint
  image: registry.gitlab.com/eyeo/docker/gitlab-codequality:21.01
  script:
    # Run checkstyle with our config against all java files
    - checkstyle --exclude=.git -c config/checkstyle/checkstyle.xml -f xml $(find . -name "*.java") -o checkstyle-report.xml || true
    # Convert the checkstyle output to code-climate format so gitlab can parse it
    - |
        violations-command-line \
          -detail-level COMPACT \
          -code-climate code-climate-report.json \
          --violations "CHECKSTYLE" "$(pwd)" ".*checkstyle-report.xml" "Checkstyle"
  artifacts:
    reports:
      codequality:
        - code-climate-report.json
```

### Known Issues

When running `violations-command-line` multiple occurrences of the following output is noted in STDERR:

```
(node:73) Warning: Accessing non-existent property 'cat' of module exports inside circular dependency
(Use `node --trace-warnings ...` to show where the warning was created)
```

This is an [issue](https://github.com/shelljs/shelljs/issues/1012) upstream in the `shelljs` library and does not
cause any problems besides the warning message.
