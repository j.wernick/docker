# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# From https://hub.docker.com/_/rust
# https://github.com/rust-lang/docker-rust/blob/e9fa22548981365ced90bc22d9173e2cf6b96890/1.53.0/buster/Dockerfile
FROM rust:1.53

# Add any components we use for linting
RUN rustup component add rustfmt \
&&  rustup component add clippy

# This is generally needed if any of your dependencies are wrapping C
# libraries and needs to build them from source.
RUN apt-get update  \
&&  apt-get install -y cmake \
&&  apt-get clean \
&&  rm -f /var/lib/apt/lists/*_*
