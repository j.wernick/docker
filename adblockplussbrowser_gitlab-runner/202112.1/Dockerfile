# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:20.04

# Settings used later on in Dockerfile
ARG ANDROID_SDK_VERSION=7583922_latest
ARG ANDROID_SDK_SHA256=124f2d5115eee365df6cf3228ffbca6fc3911d16f8025bebd5b1c6e2fcfa7faf

# Adjust environment
ENV DEBIAN_FRONTEND noninteractive
ENV ANDROID_SDK_ROOT /opt/android-sdk
ENV PATH ${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:$PATH

# Upgrade packages and install required software as provided by distribution
RUN apt-get update \
&&  apt-get -qq upgrade \
&&  apt-get -qq install \
        curl \
        git \
        openjdk-11-jdk-headless \
        unzip \
        wget \
&&  apt-get -qq autoremove \
&&  apt-get -qq clean \
&&  rm -rf /var/lib/apt/lists/*

# Installing the bare minimum Android SDK
RUN TMP=/tmp/android-sdk.zip \
;   DST=$ANDROID_SDK_ROOT/cmdline-tools \
;   wget --quiet -O $TMP https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_VERSION}.zip \
&&  echo "${ANDROID_SDK_SHA256} ${TMP}" | sha256sum -c \
&&  unzip -qq -d /tmp $TMP \
&&  mkdir -p $DST \
&&  mv /tmp/cmdline-tools $DST/latest \
&&  yes | sdkmanager --licenses > /dev/null 2>&1
