# Copyright (c) 2019-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
FROM ubuntu:18.04
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /root

#==================
# General Packages
#==================
RUN apt-get -qqy update \
&&  apt-get -qqy --no-install-recommends install \
        apt-transport-https \
        ca-certificates \
        curl \
        git \
        gnupg \
        libgconf-2-4 \
        libqt5webkit5 \
        lsb-release \
        openjdk-8-jdk \
        rinetd \
        salt-minion \
        tzdata \
        unzip \
        wget \
        xvfb \
        zip \
&&  rm -rf /var/lib/apt/lists/*

#===============
# Set JAVA_HOME
#===============
ENV JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre" \
    PATH=$PATH:$JAVA_HOME/bin

#=====================
# Install Android SDK
#=====================
ENV SDK_VERSION=sdk-tools-linux-3859397 \
    ANDROID_BUILD_TOOLS_VERSION=26.0.0 \
    ANDROID_HOME=/root \
    ANDROID_PLATFORM_VERSION=android-25

RUN wget -q -O tools.zip https://dl.google.com/android/repository/${SDK_VERSION}.zip \
&&  unzip -q tools.zip && rm tools.zip \
&&  chmod a+x -R $ANDROID_HOME \
&&  chown -R root:root $ANDROID_HOME

ENV PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin

# https://askubuntu.com/questions/885658/android-sdk-repositories-cfg-could-not-be-loaded
RUN mkdir -p ~/.android \
&&  touch ~/.android/repositories.cfg \
&&  echo y | sdkmanager "platform-tools" \
&&  echo y | sdkmanager "build-tools;$ANDROID_BUILD_TOOLS_VERSION" \
&&  echo y | sdkmanager "platforms;$ANDROID_PLATFORM_VERSION"

ENV PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools

#====================================
# Install latest nodejs, npm, appium
#====================================
ENV APPIUM_VERSION=1.16.0

ADD nodesource.list /etc/apt/sources.list.d/
ADD nodesource.gpg.key /tmp/
RUN apt-key add /tmp/nodesource.gpg.key \
&&  apt-get update \
&&  apt-get -qqy install nodejs \
&&  npm install -g appium@${APPIUM_VERSION} --unsafe-perm=true --allow-root \
&&  exit 0 \
&&  npm cache clean \
&&  apt-get remove --purge -y npm \
&&  apt-get autoremove --purge -y \
&&  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#================================
# APPIUM Test Distribution (ATD)
#================================
ENV ATD_VERSION=1.2
RUN wget -nv -O RemoteAppiumManager.jar \
      "https://github.com/AppiumTestDistribution/ATD-Remote/releases/download/${ATD_VERSION}/RemoteAppiumManager-${ATD_VERSION}.jar"

#========================================================================================================
# Install OpenCV for NodeJS for image comparison used in the tests
# The automatic build via npm adds 2Gb to the image size so the build process has been adopted from
# https://github.com/justadudewhohacks/opencv4nodejs-docker-images/blob/master/opencv-nodejs/Dockerfile
#========================================================================================================
ENV OPENCV4NODEJS_DISABLE_AUTOBUILD=1 \
    OPENCV_VERSION=3.4.1

RUN apt-get install -y build-essential cmake \
&&  mkdir opencv \
&&  cd opencv \
&&  wget -q -O opencv-${OPENCV_VERSION}.zip \
      https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip \
&&  unzip -q opencv-${OPENCV_VERSION}.zip \
&&  mkdir opencv-${OPENCV_VERSION}/build \
&&  cd opencv-${OPENCV_VERSION}/build \
&&  cmake_flags="   -D CMAKE_BUILD_TYPE=RELEASE \
                    -D BUILD_EXAMPLES=OFF \
                    -D BUILD_DOCS=OFF \
                    -D BUILD_TESTS=OFF \
                    -D BUILD_PERF_TESTS=OFF \
                    -D BUILD_JAVA=OFF \
                    -D BUILD_opencv_apps=OFF \
                    -D BUILD_opencv_aruco=OFF \
                    -D BUILD_opencv_bgsegm=OFF \
                    -D BUILD_opencv_bioinspired=OFF \
                    -D BUILD_opencv_ccalib=OFF \
                    -D BUILD_opencv_datasets=OFF \
                    -D BUILD_opencv_dnn_objdetect=OFF \
                    -D BUILD_opencv_dpm=OFF \
                    -D BUILD_opencv_fuzzy=OFF \
                    -D BUILD_opencv_hfs=OFF \
                    -D BUILD_opencv_java_bindings_generator=OFF \
                    -D BUILD_opencv_js=OFF \
                    -D BUILD_opencv_img_hash=OFF \
                    -D BUILD_opencv_line_descriptor=OFF \
                    -D BUILD_opencv_optflow=OFF \
                    -D BUILD_opencv_phase_unwrapping=OFF \
                    -D BUILD_opencv_python3=OFF \
                    -D BUILD_opencv_python_bindings_generator=OFF \
                    -D BUILD_opencv_reg=OFF \
                    -D BUILD_opencv_rgbd=OFF \
                    -D BUILD_opencv_saliency=OFF \
                    -D BUILD_opencv_shape=OFF \
                    -D BUILD_opencv_stereo=OFF \
                    -D BUILD_opencv_stitching=OFF \
                    -D BUILD_opencv_structured_light=OFF \
                    -D BUILD_opencv_superres=OFF \
                    -D BUILD_opencv_surface_matching=OFF \
                    -D BUILD_opencv_ts=OFF \
                    -D BUILD_opencv_xobjdetect=OFF \
                    -D BUILD_opencv_xphoto=OFF \
                    -D CMAKE_INSTALL_PREFIX=/usr/local" \
&&  cmake $cmake_flags .. \
&&  make -j $(nproc) \
&&  make install \
&&  sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf' \
&&  ldconfig \
&&  cd ../../../ \
&&  rm -rf opencv \
&&  cd /usr/lib/node_modules/appium \
&&  npm install opencv4nodejs \
&&  apt-get purge -y build-essential cmake \
&&  apt-get autoremove -y --purge \
&&  rm -rf /var/lib/apt/lists/*

#============================================
# Download chromedrivers and create map.json
#============================================
RUN mkdir /chromedriver \
&&  cd /chromedriver/ \
&&  echo '{' > /chromedriver/map.json \
&&  for MAJOR_VERSION in $(seq 77 114); \
    do if [ "$MAJOR_VERSION" -eq 82 ] ; then echo "Skipping non-existant chrome 82"; continue ; fi \
&&  echo "Downloading chromedriver for chrome ${MAJOR_VERSION}" \
&&  LATEST_VERSION=$(curl -f https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${MAJOR_VERSION}) \
&&  wget -q "https://chromedriver.storage.googleapis.com/${LATEST_VERSION}/chromedriver_linux64.zip" \
&&  unzip -q chromedriver_linux64.zip \
&&  rm -f chromedriver_linux64.zip \
&&  rm -f LICENSE.chromedriver \
&&  mv chromedriver /chromedriver/${MAJOR_VERSION} \
&&  echo """\"$(/chromedriver/${MAJOR_VERSION} --version | awk {'print $2'})\": \"${MAJOR_VERSION}\",""" >> /chromedriver/map.json; \
    done \
&&  sed -i -e '$s/,//g' /chromedriver/map.json \
&&  echo '}' >> /chromedriver/map.json

#==================================
# Fix Issue with timezone mismatch
#==================================
ENV TZ="Europe/Berlin"
RUN echo "${TZ}" > /etc/timezone

#===============
# Expose Ports
#---------------
# 4723
#   Appium port
# 4567
#   ATD port
#===============
EXPOSE 4723
EXPOSE 4567

#====================================================
# Scripts to run appium and connect to Selenium Grid
#====================================================
COPY entry_point.sh \
     generate_config.sh \
     wireless_connect.sh \
     wireless_autoconnect.sh \
     /root/

RUN chmod +x /root/entry_point.sh \
&&  chmod +x /root/generate_config.sh \
&&  chmod +x /root/wireless_connect.sh \
&&  chmod +x /root/wireless_autoconnect.sh

#========================================
# Run xvfb and appium server
#========================================
CMD /root/wireless_autoconnect.sh && /root/entry_point.sh
