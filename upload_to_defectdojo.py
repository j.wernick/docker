#!/usr/bin/python3

#This is a python script developed by Malek Mohammad from the security team, to enable \
#development teams to upload vulnerability reports to DefectDojo

import argparse
from datetime import datetime
import json
import os
import requests
import urllib3
import sys
urllib3.disable_warnings()

#upload the scan report to defect dojo
def upload_results(import_url, api_key, scanner, result_file, engagement_id, lead_id, environment, product_name, verify=False): # set verify to False if ssl cert is self-signed
    AUTH_TOKEN = "Token "+api_key

    headers = dict()
    json_data = dict()
    files = dict()

    # Prepare headers
    # headers = {'Authorization': 'Token 3e24a3ee5af0305af20a5e6224052de3ed2f6859'}
    headers['Authorization'] = AUTH_TOKEN

    # Prepare JSON data to send to API
    json_data['minimum_severity'] = "Medium"
    json_data['scan_date'] = datetime.now().strftime("%Y-%m-%d")
    json_data['verified'] = verify
    json_data['active'] = True
    json_data['engagement'] = engagement_id
    json_data['lead'] = lead_id
    json_data['scan_type'] = scanner
    json_data['environment'] = environment
    json_data['product_name'] = product_name
    print(json_data)

    # Prepare file data to send to API
    files['file'] = open(result_file)

    # Make a request to API
    response = requests.post(import_url, headers=headers, files=files, data=json_data, verify=verify)

    #print("#######DEBUG##########")
    #print("Response Text from Defectdojo Upload...")
    #print(response.text)

    return response.status_code

#list engagements on defect dojo with criteria
def list_engagements(api_url, api_key, product=None, status=None, name=None):
    url = api_url + '/engagements/'
    headers = {'Authorization': f'Token {api_key}'}
    params = {}
    if product:
        params['product'] = product
    if status:
        params['status'] = status
    if name:
        params['name'] = name
    response = requests.get(url, headers=headers, params=params)
    response.raise_for_status()

    data = response.json()
    results = data['results']

    if results:
        engagement_id = results[0]['id']
        return engagement_id
    else:
        return -1

#main function with options
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CI/CD integration for DefectDojo')
    parser.add_argument('--host', help="DefectDojo Hostname", required=True)
    parser.add_argument('--port', help="DefectDojo Port", required=True)
    parser.add_argument('--api_key', help="API v2 Key", required=True)
    parser.add_argument('--engagement_id', help="Engagement ID", required=False)
    parser.add_argument('--result_file', help="Scanner file", required=False)
    parser.add_argument('--scanner', help="Type of scanner", required=False)
    parser.add_argument('--product_id', help="DefectDojo Product ID", required=False)
    parser.add_argument('--lead_id', help="ID of the user conducting the testing", required=False)
    parser.add_argument('--environment', help="Environment name", required=False)
    parser.add_argument('--build_id', help="Reference to external build id", required=False)
    parser.add_argument('--product_name', help="Product Name", required=False)
    parser.add_argument('--action', help="Action to perform: upload, list or tupload", required=True)
    parser.add_argument('--engagement_name', help="Engagement Name", required=False)

    #parsing args
    args = vars(parser.parse_args())
    host = args["host"]
    port = args["port"]
    api_key = args["api_key"]
    action = args["action"]
    engagement_name = args["engagement_name"]

    API_URL = "https://"+host+":"+port+"/api/v2"

    # uploading report or targeted upload to an engagement
    if action == "upload" or action == "tupload":
        engagement_id = args["engagement_id"]
        result_file = args["result_file"]
        IMPORT_SCAN_URL = API_URL+"/import-scan/"
        product_name = args["product_name"]
        scanner = args["scanner"]
        lead_id = args["lead_id"]
        environment = args["environment"]

        #getting the targeted engagement id
        if action == "tupload":
            if not engagement_name:
                print("Engagement name cannot be empty!")
                sys.exit(1)
            engagement_id = list_engagements(API_URL, api_key, name=engagement_name)
            if engagement_id == -1:
                engagement_id = 30

        #validations
        if not engagement_id:
            print("Engagement ID variable cannot be empty!")
            sys.exit(1)
        if not result_file:
            print("Result file variable cannot be empty!")
            sys.exit(1)
        if not product_name:
            print("Product name variable cannot be empty!")
            sys.exit(1)
        if not scanner:
            print("Scanner variable cannot be empty!")
            sys.exit(1)
        if not lead_id:
            print("Lead ID variable cannot be empty!")
            sys.exit(1)
        if not environment:
            print("Environment variable cannot be empty!")
            sys.exit(1)

        #uploading to DD
        result = upload_results(IMPORT_SCAN_URL, api_key, scanner, result_file, engagement_id, lead_id, environment, product_name, True)
        if result == 201 :
            print("Successfully uploaded the results to Defect Dojo")
        else:
            print("Something went wrong, please debug " + str(result))
    
    #listing engagements
    elif action == "list":
        if not engagement_name:
            print("Engagement name cannot be empty!")
            sys.exit(1)
        engagement_id = list_engagements(API_URL, api_key, name=engagement_name)
        if engagement_id != -1:
            print(f"Engagement found with ID: {engagement_id}")
        else:
            print("No engagement found with the given name.")

    else:
        print("Invalid action specified. Must be 'upload', 'list', or 'tupload'.")
