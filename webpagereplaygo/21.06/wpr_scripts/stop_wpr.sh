#!/bin/bash
set -e
# This script should be called after the tests have completed.
# It starts a graceful shutdown of wpr by writing to a specific file.
# See the entrypoint.sh for the other side of the coin.

echo "$(date) WPR is in \"$(cat "${CI_PROJECT_DIR}"/wpr_scripts/wpr_state)\" mode. Requesting stop.."
echo kill > "${CI_PROJECT_DIR}"/wpr_scripts/wpr_state

while [ -f "${CI_PROJECT_DIR}"/wpr_scripts/wpr_state ]
do
    echo "$(date) WPR is: $(cat "${CI_PROJECT_DIR}"/wpr_scripts/wpr_state)"
    sleep 1
done

echo "$(date) wpr_state file removed, wpr is stopped"
