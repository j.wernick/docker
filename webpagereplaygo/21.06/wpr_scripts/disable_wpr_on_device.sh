#!/bin/bash
set -e
# This script is responsible for connecting to the android device
# and removing the iptables rules that redirect traffic to wpr.
# The device should be left in the state it was in before running
# the corresponding enable_wpr_on_device.sh

# Reconnect if we are using adb over tcp.
# ADB_TCP_DEVICE is a file created by enable_wpr_on_device.sh
if [ -f ADB_TCP_DEVICE ]
then
    ADB_TCP_DEVICE=$(cat ADB_TCP_DEVICE)
    echo "Reconnecting to wirelss adb device ${ADB_TCP_DEVICE}"
    adb connect "${ADB_TCP_DEVICE}"
    sleep 5
fi

# Check root status (if adb root is possible its already running)
if ! adb shell id | grep root
then
    SU="su -c"
fi

echo "Removing wpr iptables rules from device"
adb shell "${SU} iptables -t nat -D OUTPUT -p tcp --dport 80 -j DNAT --to-destination 127.0.0.1:9080"
adb shell "${SU} iptables -t nat -D OUTPUT -p tcp --dport 443 -j DNAT --to-destination 127.0.0.1:9081"

echo "iptables config (iptables -t nat -L OUTPUT):"
adb shell "${SU} iptables -t nat -L OUTPUT"

# check the ports again and fail if either ports are still forwarding
adb shell "${SU} iptables -t nat -L OUTPUT" | grep -q "9080" && exit 1
adb shell "${SU} iptables -t nat -L OUTPUT" | grep -q "9081" && exit 1
