#!/bin/bash
set -e

# Check we want to enable wpr (its not an error if we dont)
if [ -z "${WPR_FILE}" ]
then
    echo "WARNING: NOT ENABLING WPR AS \${WPR_FILE} is not set."
    exit
fi

# detect if we are connected to the device via tcp
ADB_TCP_DEVICE=$(adb devices | grep ":" | awk '{print $1}')

echo "Attempting to restart adb as root..."
adb root && sleep 5

# reconnect to the tcp device after restarting adbd
if [ "${ADB_TCP_DEVICE}" ]
then
    echo "Reconnecting to tcp adb device ${ADB_TCP_DEVICE}"
    echo "${ADB_TCP_DEVICE}" > ADB_TCP_DEVICE # read by disable_wpr_on_device.sh
    adb connect "$ADB_TCP_DEVICE"
fi

# Make sure we have root
adb wait-for-device
if ! adb shell id | grep root
then
    echo "adb root unsupported, trying with su"
    if [[ $(adb shell which su) ]]
    then
        SU="su -c"
    else
        echo "su not found either. Exiting, you need a rooted device!"
        exit 1
    fi
fi

# Create a tunnel from the container running the tests to the container
# running wpr (this script is executed from the former)
if ! which socat > /dev/null
then
    apt-get update
    apt-get install -qy socat
fi
socat tcp-l:9080,fork,reuseaddr tcp:"${WPR_CONTAINER:-wpr}":9080 &
socat tcp-l:9081,fork,reuseaddr tcp:"${WPR_CONTAINER:-wpr}":9081 &

# Now forward traffic from ports on the device to the test container
echo "Configuring on-device adb tunnels for wpr"
adb reverse tcp:9080 tcp:9080
adb reverse tcp:9081 tcp:9081

# Now redirect any outbound traffic via iptables.
# This may not be necessary in all cases (eg chromium can do this by
# enabling the `host-resolver-rules` flag)
if [ -z "${SKIP_IPTABLES_RULES}" ]
then
    echo "Configuring on-device iptables rules"
    adb shell "${SU} iptables -t nat -A OUTPUT -p tcp --dport 80 -j DNAT --to-destination 127.0.0.1:9080"
    adb shell "${SU} iptables -t nat -A OUTPUT -p tcp --dport 443 -j DNAT --to-destination 127.0.0.1:9081"

    echo "iptables config (iptables -t nat -L OUTPUT):"
    adb shell "${SU} iptables -t nat -L OUTPUT"

    # check the ports again and error if either ports are not forwarding
    adb shell "${SU} iptables -t nat -L" | grep -q "9080" || exit 1
    adb shell "${SU} iptables -t nat -L" | grep -q "9081" || exit 1
fi

# Wait for wpr to start - it can take a few seconds, if the repo is big
while [ ! -f "${OUTPUT_DIR}"/wpr_scripts/wpr_state ] && [ "${waittime:-0}" -lt 60 ]
do
    echo "Waiting for wpr to start..."
    ((waittime=waittime+1))
    sleep 1
done

# At this point, outbound http(s) traffic is redirected on the device (via iptables) to the
# container running the tests over the adb tunnels. Then to the wpr container (via socat)
echo "Tunnels running and WPR is: \"$(cat "${CI_PROJECT_DIR}"/wpr_scripts/wpr_state)\""
