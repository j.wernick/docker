#!/usr/bin/bash

# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script serves as the entrypoint to the icecc container and
# the ICECC_ROLE env var should be set to a supported role (below).
# Any arguments passed into the container will be passed onto the
# underlying command. Pass in `--help` to see available options.
set -e

if [ "$ICECC_ROLE" == "SCHEDULER" ]; then
	/usr/sbin/icecc-scheduler $@
elif [ "$ICECC_ROLE" == "MONITOR" ]; then
	/usr/bin/icecream-sundae $@
elif [ "$ICECC_ROLE" == "WORKER" ]; then
	/usr/sbin/iceccd $@
else
	echo "Set \$ICECC_ROLE to either SCHEDULER, MONITOR, or WORKER"
	exit 1
fi
