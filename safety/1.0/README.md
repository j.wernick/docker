## **Safety**
This contains the Dockerfile and image for running Safety tool, Safety checks Python dependencies for known security vulnerabilities and suggests the proper remediations for vulnerabilities detected. Safety can be run on developer machines, in CI/CD pipelines and on production systems.

## **How to run**
docker run --rm -v $(pwd):/src registry.gitlab.com/eyeo/docker/safety check -r /src/requirements.txt --json > oast-safety-results.json
